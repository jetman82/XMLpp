// ConsoleApplication1.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include "XMLDocument.h"

int main(int argc, char** argv)
{
	if (argc > 2)
	{
		return 0;
	}

	XMLErr err;


	std::ifstream in("Mansion.RS.xml");

	XMLDocument doc(ReadFlags::IgnoreComments | ReadFlags::IgnoreDecl);
	std::ostringstream contents;
	contents << in.rdbuf();

	std::string inText = contents.str();
	ERR result = doc.Parse(inText);
	if (result != ERR::ERR_OK)
	{
		std::cout << "Error parsing document :  " << err.GetErrInfo(result) << std::endl;
	}
	else
	{
		std::cout << "Parsing completed" << std::endl;
	}

	system("PAUSE");

	contents.clear();
	
	return 0;
}

